import {
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewEncapsulation
} from "@angular/core";
import * as go from "gojs";
import {
  DataSyncService,
  DiagramComponent,
  PaletteComponent
} from "gojs-angular";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  public diagramNodeData: Array<go.ObjectData> = [
    {
      key: 1,
      name: "Server",
      inservices: [{ name: "s1" }],
      outservices: [{ name: "o1" }]
    },
    {
      key: 2,
      name: "Server",
      inservices: [{ name: "s1" }],
      outservices: [{ name: "o1" }]
    },
    {
      key: 3,
      name: "Server",
      inservices: [{ name: "s1" }],
      outservices: [{ name: "o1" }]
    },
    {
      key: 4,
      name: "Server",
      inservices: [{ name: "s1" }],
      outservices: [{ name: "o1" }]
    }
  ];
  public diagramLinkData: Array<go.ObjectData> = [
    { from: 1, fromPort: "o1", to: 2, toPort: "s1" }
  ];
  public diagramDivClassName: string = "diagram-div";
  public jsonValue: string = "";

  constructor(private changeDetector: ChangeDetectorRef) {}

  public ngOnInit(): void {}

  public addFigure() {
    var key = this.diagramNodeData[this.diagramNodeData.length - 1].key + 1;

    this.diagramNodeData.push({
      key: key,
      name: "New" + key,
      inservices: [{ name: "s1" }],
      outservices: [{ name: "o1" }]
    });
    this.changeDetector.detectChanges();
  }

  public diagramModelChange(changes: go.IncrementalData) {
    this.diagramNodeData = DataSyncService.syncNodeData(
      changes,
      this.diagramNodeData
    );
    this.diagramLinkData = DataSyncService.syncLinkData(
      changes,
      this.diagramLinkData
    );
    this.jsonValue = this.convertToJson(
      this.diagramNodeData,
      this.diagramLinkData
    );
  }

  public convertToJson(nodes, links): string {
    return JSON.stringify({
      nodes: nodes,
      links: links
    });
  }

  public onJsonChange($event) {
    var data = JSON.parse($event);
    if (this.checkData(data)) {
      this.diagramNodeData = data.nodes;
      this.diagramLinkData = data.links;
      this.changeDetector.detectChanges();
    }
  }

  private checkData(data: any): boolean {
    if (!data.nodes && !(data.nodes instanceof Array)) return false;
    for (const item of data.nodes) {
      if (!item.key && !item.name && !item.inservices && !item.outservices)
        return false;
    }

    if (!data.links && !(data.links instanceof Array)) return false;
    for (const item of data.links) {
      if (!item.from && !item.fromPort && !item.to && !item.toPort)
        return false;
    }
    return true;
  }

  public initDiagram(): go.Diagram {
    const $ = go.GraphObject.make;
    const diagram = $(go.Diagram, {
      "undoManager.isEnabled": true,
      model: $(go.GraphLinksModel, {
        linkKeyProperty: "key"
      })
    });

    var UnselectedBrush = "lightgray";
    var SelectedBrush = "dodgerblue";
    function makeItemTemplate(leftside) {
      return $(
        go.Panel,
        "Auto",
        { margin: new go.Margin(1, 0) },
        $(
          go.Shape,
          {
            name: "SHAPE",
            fill: UnselectedBrush,
            stroke: "red",
            geometryString:
              "M409.133,109.203c-19.608-33.592-46.205-60.189-79.798-79.796C295.736,9.801,259.058,0,219.273,0 c-39.781,0-76.47,9.801-110.063,29.407c-33.595,19.604-60.192,46.201-79.8,79.796C9.801,142.8,0,179.489,0,219.267 c0,39.78,9.804,76.463,29.407,110.062c19.607,33.592,46.204,60.189,79.799,79.798c33.597,19.605,70.283,29.407,110.063,29.407 s76.47-9.802,110.065-29.407c33.593-19.602,60.189-46.206,79.795-79.798c19.603-33.596,29.403-70.284,29.403-110.062 C438.533,179.485,428.732,142.795,409.133,109.203z M353.742,297.208c-13.894,23.791-32.736,42.633-56.527,56.534 c-23.791,13.894-49.771,20.834-77.945,20.834c-28.167,0-54.149-6.94-77.943-20.834c-23.791-13.901-42.633-32.743-56.527-56.534 c-13.897-23.791-20.843-49.772-20.843-77.941c0-28.171,6.949-54.152,20.843-77.943c13.891-23.791,32.738-42.637,56.527-56.53 c23.791-13.895,49.772-20.84,77.943-20.84c28.173,0,54.154,6.945,77.945,20.84c23.791,13.894,42.634,32.739,56.527,56.53 c13.895,23.791,20.838,49.772,20.838,77.943C374.58,247.436,367.637,273.417,353.742,297.208z",
            spot1: new go.Spot(2, 2, 3, 3),
            toSpot: go.Spot.Left,
            toLinkable: leftside,
            fromSpot: go.Spot.Right,
            fromLinkable: !leftside,
            cursor: "pointer"
          },
          new go.Binding("portId", "name")
        ),
        $(go.TextBlock, new go.Binding("text", "name"), {
          isActionable: true,
          click: function(e, tb) {
            var shape = tb.panel.findObject("SHAPE");
            if (shape !== null) {
              var oldskips = shape.diagram.skipsUndoManager;
              shape.diagram.skipsUndoManager = true;
              if (shape.fill === UnselectedBrush) {
                shape.fill = SelectedBrush;
              } else {
                shape.fill = UnselectedBrush;
              }
              shape.diagram.skipsUndoManager = oldskips;
            }
          }
        })
      );
    }

    diagram.nodeTemplate = $(
      go.Node,
      "Spot",
      { selectionAdorned: false },
      { locationSpot: go.Spot.Center, locationObjectName: "BODY" },
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
        go.Point.stringify
      ),
      $(
        go.Panel,
        "Auto",
        { name: "BODY" },
        $(
          go.Shape,
          "RoundedRectangle",
          { stroke: "gray", strokeWidth: 2, fill: "transparent" },
          new go.Binding("stroke", "isSelected", function(b) {
            return b ? SelectedBrush : UnselectedBrush;
          }).ofObject()
        ),
        $(
          go.Panel,
          "Vertical",
          { margin: 26 },
          $(go.TextBlock, { editable: true }, new go.Binding("text", "name"), {
            alignment: go.Spot.Center
          })
        )
      ),
      $(
        go.Panel,
        "Vertical",
        { name: "LEFTPORTS", alignment: new go.Spot(0, 0.5, 0, 7) },
        new go.Binding("itemArray", "inservices"),
        { itemTemplate: makeItemTemplate(true) }
      ),
      $(
        go.Panel,
        "Vertical",
        { name: "RIGHTPORTS", alignment: new go.Spot(1, 0.5, 0, 7) },
        new go.Binding("itemArray", "outservices"),
        { itemTemplate: makeItemTemplate(false) }
      )
    );

    diagram.linkTemplate = $(
      go.Link,
      { routing: go.Link.Orthogonal, corner: 10, toShortLength: -3 },
      {
        relinkableFrom: true,
        relinkableTo: true,
        reshapable: true,
        resegmentable: true
      },
      $(go.Shape, { stroke: "gray", strokeWidth: 2.5 })
    );

    function findAllSelectedItems() {
      var items = [];
      for (var nit = diagram.nodes; nit.next(); ) {
        var node = nit.value;
        var table = <any>node.findObject("LEFTPORTS");
        if (table !== null) {
          for (var iit = table.elements; iit.next(); ) {
            var itempanel = iit.value;
            var shape = itempanel.findObject("SHAPE");
            if (shape !== null && shape.fill === SelectedBrush)
              items.push(itempanel);
          }
        }
        table = node.findObject("RIGHTPORTS");
        if (table !== null) {
          for (var iit = table.elements; iit.next(); ) {
            var itempanel = iit.value;
            var shape = itempanel.findObject("SHAPE");
            if (shape !== null && shape.fill === SelectedBrush)
              items.push(itempanel);
          }
        }
      }
      return items;
    }

    diagram.commandHandler.canDeleteSelection = function() {
      return (
        go.CommandHandler.prototype.canDeleteSelection.call(
          diagram.commandHandler
        ) || findAllSelectedItems().length > 0
      );
    };

    diagram.commandHandler.deleteSelection = function() {
      var items = findAllSelectedItems();
      if (items.length > 0) {
        diagram.startTransaction("delete items");
        for (var i = 0; i < items.length; i++) {
          var item = items[i];
          var nodedata = item.part.data;
          var itemdata = item.data;
          var itemarray = nodedata.inservices;
          var itemindex = itemarray.indexOf(itemdata);
          if (itemindex < 0) {
            itemarray = nodedata.outservices;
            itemindex = itemarray.indexOf(itemdata);
          }
          if (itemindex >= 0) {
            diagram.model.removeArrayItem(itemarray, itemindex);
          }
        }
        diagram.commitTransaction("delete items");
      } else {
        go.CommandHandler.prototype.deleteSelection.call(
          diagram.commandHandler
        );
      }
    };

    return diagram;
  }
}
